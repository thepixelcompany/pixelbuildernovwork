import Vue from 'vue'
import Vuex from 'vuex'
import EventBus from './event-bus.js'
const generate = require('nanoid/generate')
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isEdit: 1,
    showAppStore: 0,
    showLogoMenu: 0,
    showChatModal: 0,
    showExportModal: 0,
    showComingSoonModal: 0,
    showPublishMenu: 0,
    showPageDrawer: 0,
    showMediaDrawer: 0,
    uuid: null,
    widgets: [],
    activeWidget: {}
  },
  /***************************************************************************************/
  mutations: {
    addWidget (state, { data: data = null, params }) {
      let where
      if (params.where === undefined) {
        where = null
      } else {
        where = params.where
      }
      let def = { uuid: generate('1234567890abcdef', 8), parent: params.parent, where: where }
      let setting = params.item
      if (data !== null) {
        setting.src = data
      }
      state.widgets.push(Object.assign(setting, def))
      if (setting.type === 'rich-text-comp') {
        params.childItems.forEach(i => {
          let def = {uuid: generate('1234567890abcdef', 8), parent:setting.uuid, where: where}
          state.widgets.push(Object.assign(i, def))
        })
      }
    },
    select (state, payload) {
      state.uuid = payload.uuid
      let widget = state.widgets.find(i => i.uuid === state.uuid)
      if (state.uuid !== -1) {
        state.activeWidget = widget
      }
    },
    clone (state) {
      let clone = Object.assign({}, state.activeWidget, { uuid: generate('1234567890abcdef', 8) })
      state.widgets.push(clone)
    },
    remove (state) {
      state.widgets.forEach((i, j) => {
        if (i.uuid === state.activeWidget.uuid) {
          state.widgets.splice(j, 1)
          state.uuid = -1
        }
      })
    },
    updateWidget (state, payload) {
      state.activeWidget.parent = payload.params.parent
      state.activeWidget.where = payload.params.where
    },
    sortWidget(state, params) {
      let widgets = state.widgets.slice(0, state.widgets.length)
      let index1 = widgets.findIndex(i => i.uuid === params.item.uuid)
      let originindex2 = widgets.findIndex(i => i.uuid === params.after.uuid)
      widgets.splice(index1, 1)
      let index2 = widgets.findIndex(i => i.uuid === params.after.uuid)      
      if (index1 > originindex2) {
        widgets.splice(index2, 0, params.item)
      } else {
        widgets.splice(index2 + 1, 0, params.item)
      }
      state.widgets = widgets
    },
    updateData(state, params) {
      let element = state.widgets.find(i => i.uuid === params.id)
      element[params.key] = params.value
    }
  },
  /*******************************************************************************************/
  actions: {
    addWidget ({ state, commit, store }, params) {
      if (params.item.isUpload) {
        EventBus.$emit('upload', (payload) => {
          commit('addWidget', { data: payload, params })
          commit('select', { uuid: state.widgets[state.widgets.length - 1].uuid })
        }, true)
      } else {
        commit('addWidget', { params })
        commit('select', { uuid: state.widgets[state.widgets.length - 1].uuid })
      }
    },
    updateWidget ({state, commit}, params) {
      commit('updateWidget', {params})
    },
    unselect ({ state, commit, store }) {
      commit('select', { uuid: -1 })
    }
  }
})
